#!/bin/bash

#convert hql to sql standard
find hql/. -type f -name "*.hql" -print0 | xargs -0 \
sed -i -z -E \
'
s/(\$\(hiveconf:environemnt\)\w+)\.(\w+)/`'$PROJECT'.'$dataset'.\2`/g;
s/CREATE\s+EXTERNAL\s+TABLE/CREATE TABLE/g;
s/(\n|\r)\s*\)\s*(\n|\r).*(\n*\r*\n*.*)*/\n);/g
'

#merge all content to one file
find hql/. -type f -name "*.hql" | xargs cat > ./union.sql

