DROP TABLE IF EXISTS `burnished-case-280710.terraform_ex_dataset.ST2`;
DROP TABLE IF EXISTS `burnished-case-280710.terraform_ex_dataset.ST3`;
DROP TABLE IF EXISTS `burnished-case-280710.terraform_ex_dataset.ST22`;
CREATE TABLE `burnished-case-280710.terraform_ex_dataset.ST22`
(
    `ADDRESS_ID` STRING,
     `INDIVIDUAL_ID` STRING,
      `FIRST_NAME` STRING,
       `LAST_NAME` STRING
);
